import {Component} from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";
import {productsData} from "../../data";

@Component({
  selector: 'app-bai3',
  standalone: true,
  imports: [
    MainLayoutComponent
  ],
  templateUrl: './bai3.component.html',
  styleUrl: './bai3.component.css'
})
export class Bai3Component {
  filter = ''
  products: IProduct[] = productsData
}
