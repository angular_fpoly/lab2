import {Component, EventEmitter, Input, Output} from '@angular/core';
import {productsData} from "../../data";
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";

@Component({
  selector: 'app-bai5',
  standalone: true,
  imports: [
    MainLayoutComponent
  ],
  templateUrl: './bai5.component.html',
  styleUrl: './bai5.component.css'
})
export class Bai5Component {
  isHiddenImage: boolean = true;
  private products: IProduct[] = productsData;
  @Input()
  filter: string = '';
  @Output()
  filterChange = new EventEmitter<string>();

  set setProductList(productList: IProduct[]) {
     this.products = productList;
  }

  get getProductList(): IProduct[] {
    return this.products;
  }

  filterOnChange(event: Event): void {
    this.filter = (event.target as HTMLInputElement).value;
    this.filterChange.emit(this.filter);

    if (this.filter !== '') {
      const filterValue = this.filter.trim().toLowerCase();
      this.setProductList = productsData.filter(product =>
        product.productName.toLowerCase().includes(filterValue)
      );
      if (this.products.length === 0) {
        this.setProductList = productsData;
      }
    } else {
      this.setProductList = productsData;
    }
  }

  hiddenImageEvent() {
    this.isHiddenImage = !this.isHiddenImage;
  }
}
