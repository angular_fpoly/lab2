import {Component, Output, Input, EventEmitter} from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";
import {productsData} from "../../data";

@Component({
  selector: 'app-bai4',
  standalone: true,
  imports: [
    MainLayoutComponent
  ],
  templateUrl: './bai4.component.html',
  styleUrl: './bai4.component.css'
})
export class Bai4Component {
  products: IProduct[] = productsData;
  @Input()
  filter: string = '';
  @Output()
  filterChange = new EventEmitter<string>();

  filterOnChange(event: Event): void {
    this.filter = (event.target as HTMLInputElement).value;
    this.filterChange.emit(this.filter);

    if (this.filter !== '') {
      const filterValue = this.filter.trim().toLowerCase();
      this.products = productsData.filter(product =>
        product.productName.toLowerCase().includes(filterValue)
      );
      if (this.products.length === 0) {
        this.products = productsData;
      }
    } else {
      this.products = productsData;
    }
  }
}
