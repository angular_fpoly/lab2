import { Component } from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";
import {productsData} from "../../data";

@Component({
  selector: 'app-bai2',
  standalone: true,
  imports: [
    MainLayoutComponent
  ],
  templateUrl: './bai2.component.html',
  styleUrl: './bai2.component.css'
})
export class Bai2Component {
  filter = ''
  products: IProduct[] = productsData
}
